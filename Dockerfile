FROM python:3.9.5-slim

ENV PYTHONUNBUFFERED 1

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install build-essential git -y

RUN pip install --upgrade pip

RUN mkdir /app
WORKDIR /app/maybax

COPY ./requirements/requirements.txt ./requirements/requirements.txt

RUN pip install -r ./requirements/requirements.txt

# CMD ["python", "maybax/run.py"]

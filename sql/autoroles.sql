BEGIN;
--
-- Create model AutoRoles
--
CREATE TABLE "autoroles_autoroles" ("id" bigserial NOT NULL PRIMARY KEY, "emoji" varchar(10) NOT NULL, "role_id" bigint NOT NULL, "role_type" varchar(10) NOT NULL, "guild_id" bigint NOT NULL UNIQUE);
ALTER TABLE "autoroles_autoroles" ADD CONSTRAINT "autoroles_autoroles_guild_id_97f5f3aa_fk_guildowne" FOREIGN KEY ("guild_id") REFERENCES "guildownersettings_guildownersettings" ("id") DEFERRABLE INITIALLY DEFERRED;
COMMIT;


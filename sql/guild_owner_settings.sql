BEGIN;
--
-- Create model GuildOwnerSettings
--
CREATE TABLE "guildownersettings_guildownersettings" ("id" bigserial NOT NULL PRIMARY KEY, "guild_id" bigint NOT NULL UNIQUE, "logging_channel" bigint NULL, "vc_logging_channel" bigint NULL, "mod_logging_channel" bigint NULL);
COMMIT;


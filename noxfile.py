import re
import subprocess

import nox

LOCATIONS = ["maybax", "noxfile.py"]

nox.options.sessions = [
    "reformat_code",
    "spell_check",
    "flake8_code",
    "pylint_code",
    "pytest_code",
]


@nox.session(python="3.9")
def compile_requirements(session: nox.Session, reuse_venv=True):
    """Use pip-tools to compile requirements for production."""
    session.run("pip-compile", "-o", "requirements/production.txt", "requirements/hikari.in", external=True)


@nox.session(python="3.9")
def compile_dev_requirements(session: nox.Session, reuse_venv=True):
    """Use pip-tools to compile requirements for development."""
    session.run(
        "pip-compile",
        "-o",
        "requirements/development.txt",
        "requirements/hikari.in",
        "requirements/dev.in",
        "requirements/flake.in",
        "requirements/pylint.in",
        "requirements/pytest.in",
        external=True,
    )


@nox.session(python="3.9")
def type_check_code(session: nox.Session, reuse_venv=True):
    """Run PyRight to Type Check code."""
    session.run("pip-compile", "-o", "requirements/development.txt", "requirements/dev.in", external=True)
    session.run("pip", "install", "-r", "requirements/development.txt")
    session.run("pyright", *LOCATIONS[:-1])


@nox.session(python="3.9")
def reformat_code(session: nox.Session, reuse_venv=True):
    """Run Black and isort to format code."""
    session.run("pip-compile", "-o", "requirements/development.txt", "requirements/dev.in", external=True)
    session.run("pip", "install", "-r", "requirements/development.txt")
    session.run("black", *LOCATIONS)
    session.run("isort", *LOCATIONS)


@nox.session(python="3.9")
def pylint_code(session: nox.Session, reuse_venv=True):
    """Run PyLint  to lint code."""
    session.run(
        "pip-compile",
        "-o",
        "requirements/development.txt",
        "requirements/dev.in",
        "requirements/pylint.in",
        external=True,
    )
    session.run("pip", "install", "-r", "requirements/development.txt")
    session.run("rm", "-rf", "./pylint", external=True)
    session.run("mkdir", "./pylint", external=True)
    pylint_result = subprocess.run(
        [
            "pylint",
            "maybax",
        ],
        capture_output=True,
    )
    pylint_result = pylint_result.stdout.decode("utf-8")
    pylint_score_re = r"^Your code has been rated at ([\d\.\/]+)"
    match = re.search(pylint_score_re, pylint_result, re.MULTILINE)
    session.run(
        "anybadge",
        "--label=Pylint",
        "--file=pylint/pylint.svg",
        f"--value={match.group(0)}",
        "2=red",
        "4=orange",
        "8=yellow",
        "10=green",
    )
    session.run("bash", "-c", f'echo "Pylint score is {match.group(0)}"', external=True)


@nox.session(python="3.9")
def flake8_code(session: nox.Session, reuse_venv=True):
    """Run Flake8  to lint code."""
    session.run(
        "pip-compile",
        "-o",
        "requirements/development.txt",
        "requirements/dev.in",
        "requirements/flake.in",
        external=True,
    )
    session.run("pip", "install", "-r", "requirements/development.txt")
    session.run("flake8", "maybax")


@nox.session(python="3.9")
def prune_code(session: nox.Session, reuse_venv=True):
    """Run Vulture  to find duplicate code."""
    session.run("pip-compile", "-o", "requirements/development.txt", "requirements/dev.in", external=True)
    session.run("pip", "install", "-r", "requirements/development.txt")
    session.run("vulture", *LOCATIONS[:-1])


@nox.session(python="3.9")
def spell_check(session: nox.Session, reuse_venv=True):
    """Run codespell to spell check code."""
    session.install("codespell")
    session.run("codespell", *LOCATIONS, "README.md")


@nox.session(python="3.9")
def pytest_code(session: nox.Session, reuse_venv=True):
    """Run pytest to test code."""
    session.run(
        "pip-compile",
        "-o",
        "requirements/development.txt",
        "requirements/dev.in",
        "requirements/pytest.in",
        external=True,
    )
    session.run("pip", "install", "-r", "requirements/development.txt")
    session.run(
        "pytest",
        *LOCATIONS,
    )
    session.run("coverage", "xml")


@nox.session(python="3.9")
def clean_up(session: nox.Session, reuse_venv=True):
    """Cleanup build directories."""
    session.run("rm", "-R", "coverage_html/", "docs/")

# Hello! I am MayBax! Your Discord Mental Health Support!

<div align="center">

[![pipeline status](https://gitlab.com/aster.codes/maybax2/badges/main/pipeline.svg)](https://gitlab.com/aster.codes/maybax2/-/pipelines)
[![coverage report](https://img.shields.io/gitlab/coverage/aster.codes/maybax2/main?label=Test%20Coverage)](https://gitlab.com/aster.codes/maybax2/-/commits/main)
[![PyLint](https://gitlab.com/aster.codes/maybax2/-/jobs/artifacts/main/raw/pylint/pylint.svg?job=PyLint%20Lint)](https://gitlab.com/aster.codes/maybax2/-/jobs/artifacts/main/raw/pylint/pylint.log?job=PyLint%20Job)
[![Black Formatted](https://img.shields.io/badge/Formatting-Black-000?logo=python&logoColor=ffd343)](https://gitlab.com/aster.codes/maybax2/-/commits/main)
[![Python Support](https://img.shields.io/badge/Python%20Support-%5E3.9.5-orange?logo=python&logoColor=ffd343)](https://gitlab.com/aster.codes/maybax2/-/commits/main)
[![Discord Support Server](https://img.shields.io/discord/794422514935529493?color=5865F2&label=Discord&logo=discord&logoColor=white)](https://discord.gg/Cx65DkJZtM)
[![Docker Compose Support](https://img.shields.io/badge/Docker%20Compose-Build%20Ready!-blue?logo=docker&logoColor=white)](https://gitlab.com/aster.codes/maybax2/-/commits/main)

</div>

> NOTE: Maybax is not intended to be a replacement for real mental health support
> or therapy. This bot is meant to provide minor help such as grounding
> exercises, support numbers, and FAQ style information about some disorders.
> That is all.

Maybax currently runs on Python 3.9.5 and the latest hikari and hikari-tanjun.

[This repository is a tutorial that will detail how Maybax was developed.](https://patchwork.systems/programming/hikari-discord-bot.html)

[This tutorial series was produced by Aster &
Katie.](https://patchwork.systems/programming/hikari-discord-bot.html) It goes
over Maybax's development and will be updated as more plugins are added!

"""Provides a SystemModel for fronting."""
from __future__ import annotations

import datetime
import json
import logging
import typing
from pathlib import Path

import aiofiles
import aiopg
import aioredis
import attr
import pytz
import tanjun
from models.front.groups import PatchworkGroup
from models.front.members import PatchworkMember
from psycopg2 import Error
from util.redis_utils import attrs_asdict_to_redis_serializer

logger = logging.getLogger(__name__)


import psycopg2.extras

SystemModelT = typing.TypeVar("SystemModelT")

M_ID = 227117043143540736


@attr.s
class SystemModel:
    """Provides a simple model for a Fronting System."""

    name: str = attr.ib()
    owner_discord_id: str = attr.ib()
    description: str = attr.ib(default="")
    tag: str = attr.ib(default="")
    avatar_url: str = attr.ib(default="")
    id_: int | None = attr.ib(default=None)
    timezone: str = attr.ib(default="America/New_York")
    created_at: datetime.datetime = attr.ib(default=datetime.datetime.now().astimezone(pytz.timezone("US/Eastern")))
    pluralkit_id: str = attr.ib(default="")
    tupper_id: int = attr.ib(default=0)
    enabled: bool = attr.ib(default=False)
    fronting: PatchworkMember | None = attr.ib(default=None)
    auto: bool = attr.ib(default=False)

    member_file: str = attr.ib(default="./data/patchwork-collective.json")
    groups_file: str = attr.ib(default="./data/patchwork-collective--groups.json")
    system_file: str = attr.ib(default="./data/patchwork-collective--system.json")
    members: typing.List[PatchworkMember] = attr.ib(factory=list)
    groups: typing.List[PatchworkGroup] = attr.ib(factory=list)

    @classmethod
    async def from_cache(cls: typing.Type[SystemModelT], member_id: int, redis: aioredis.Redis) -> SystemModelT | None:
        redis_key = f"{member_id}:system"
        redis_key_member_set = f"{member_id}:member_list"
        redis_key_group_set = f"{member_id}:group_list"
        if not await redis.exists(redis_key):
            return None
        logger.info(f"System cache exists for {member_id}")
        data = await redis.hgetall(redis_key)
        data["enabled"] = True if data["enabled"] == "1" else False
        data["auto"] = True if data["auto"] == "1" else False
        front_data = await redis.hgetall(data["fronting"])
        if front_data:
            data["fronting"] = PatchworkMember(**front_data)
        if isinstance(data.get("created_at"), str):
            try:
                data["created_at"] = datetime.datetime.fromisoformat(data["created_at"])
            except Exception:
                data["created_at"] = datetime.datetime.fromtimestamp(float(data["created_at"]))
        data["members"] = []
        data["groups"] = []
        members_list = await redis.smembers(redis_key_member_set)
        if members_list:
            logger.info("Found members in cache!")
        for part_id in members_list:
            member = await PatchworkMember.from_cache(member_id, part_id, redis)
            if member:
                data["members"].append(member)

        groups_list = await redis.smembers(redis_key_group_set)
        if groups_list:
            logger.info("Found groups in cache!")
        for group_id in groups_list:
            group = await PatchworkGroup.from_cache(member_id, group_id, redis)
            if group:
                data["groups"].append(group)
        return cls(**data)

    @classmethod
    async def setup(
        cls: typing.Type[SystemModelT],
        member_id: int,
        pg_pool: aiopg.Pool,
        redis: aioredis.Redis,
    ) -> SystemModelT | None:
        logger.info("Called SystemModel setup!")
        system_model = await cls.from_cache(member_id, redis)
        if system_model:
            logger.info("System found in cache")
            return system_model

        pg_model = await cls.setup_from_postgres(member_id, pg_pool, redis)
        if pg_model:
            logger.info("System found in Postgres")
            return pg_model
        return None

    @classmethod
    async def setup_from_json_file(cls: typing.Type[SystemModelT]) -> SystemModelT:
        logger.info("Loading System from JSON")
        with open(Path("./data/patchwork-collective--system.json"), mode="r", encoding="utf-8") as system_file:
            system_data = json.loads(system_file.read())
            logger.info("Loaded System")

        system_model = cls(**system_data)
        system_model.members_from_json_file()
        system_model.groups_from_json_file()
        return system_model

    @classmethod
    async def setup_from_postgres(
        cls: typing.Type[SystemModelT], member_id: int, pg_pool: aiopg.Pool, redis: aioredis.Redis
    ) -> SystemModelT | None:
        logger.info("Loading System from Postgres")
        db_members = []
        db_groups = []
        with await pg_pool.cursor(cursor_factory=psycopg2.extras.RealDictCursor) as cursor:
            await cursor.execute(
                """SELECT
                id as id_, tupper_id, pluralkit_id, name, description, tag, avatar_url, timezone,
                created_at, owner_discord_id
                FROM system_system
                WHERE owner_discord_id = %s LIMIT 1;""",
                (str(member_id),),
            )
            db_system = await cursor.fetchone()
            if not db_system:
                return None
            logger.info("Found System from Postgres")

            if db_system.get("id_", False):
                await cursor.execute(
                    """SELECT
                    id as id_, pluralkit_id, tupper_id, name,
                    tag, indicators, pronouns, avatar_url, birthday,
                    description, position, display_name, tupper_group_id,
                    tupper_group_id, created_at, groups_id, color
                    FROM system_member
                    WHERE system_id=%s
                    """,
                    (db_system["id_"],),
                )
                db_members = await cursor.fetchall()
                await cursor.execute(
                    """SELECT
                    id as id_, pluralkit_id, tupper_id, name,
                    tag, avatar_url, description, created_at,
                    timezone
                    FROM system_group
                    WHERE system_id=%s
                    """,
                    (db_system["id_"],),
                )
                db_groups = await cursor.fetchall()
        system_model = cls(**db_system)

        logger.info("Getting members...")
        for member in db_members:
            await system_model.add_member(member, redis)
        for group in db_groups:
            await system_model.add_group(group, redis)

        await system_model.cache(member_id, redis)

        return system_model

    async def add_member(self, member: PatchworkMember | tuple | dict, redis: aioredis.Redis):
        if isinstance(member, tuple):
            logger.info(member[-1])
            member = PatchworkMember(*list(member)[9:])
        elif isinstance(member, dict):
            logger.info(member["color"])
            member = PatchworkMember(**member)
        self.members.append(member)
        if member.id_:
            logger.info(f"Caching member {member.name} now")
            await member.cache(int(self.owner_discord_id), redis)

    async def add_group(self, group: PatchworkGroup | dict, redis: aioredis.Redis):
        if isinstance(group, dict):
            group = PatchworkGroup(**group)
        self.groups.append(group)
        if group.id_:
            logger.info(f"Caching group {group.name} now.")
            await group.cache(int(self.owner_discord_id), redis)

    async def cache(self, member_id: int, redis: aioredis.Redis):
        redis_key = f"{member_id}:system"
        EXCLUDE_FIELDS = ["member_file", "groups_file", "system_file", "members", "groups"]
        system_data = attr.asdict(
            self, filter=lambda x, _: x.name not in EXCLUDE_FIELDS, value_serializer=attrs_asdict_to_redis_serializer
        )
        if self.fronting:
            system_data["fronting"] = f"{member_id}:{self.fronting.id_}:member"
        system_data["created_at"] = self.created_at.timestamp()
        # await redis.hset(name=redis_key, mapping=system_data)
        await redis.hset(redis_key, key="id_", value=self.id_)
        await redis.hset(redis_key, key="name", value=self.name)
        await redis.hset(redis_key, key="owner_discord_id", value=self.owner_discord_id)
        await redis.hset(redis_key, key="description", value=self.description)
        await redis.hset(redis_key, key="tag", value=self.tag)
        await redis.hset(redis_key, key="avatar_url", value=self.avatar_url)
        await redis.hset(redis_key, key="timezone", value=self.timezone)
        await redis.hset(redis_key, key="created_at", value=system_data["created_at"])
        await redis.hset(redis_key, key="pluralkit_id", value=self.pluralkit_id)
        await redis.hset(redis_key, key="tupper_id", value=system_data["tupper_id"])
        await redis.hset(redis_key, key="enabled", value=system_data["enabled"])
        await redis.hset(redis_key, key="fronting", value=system_data["fronting"])
        await redis.hset(redis_key, key="auto", value=system_data["auto"])
        for member in self.members:
            await member.cache(member_id, redis)

    def members_from_json_file(self):
        logger.info("Loading Members from JSON")
        if not hasattr(self, "members") or not bool(self.members):
            with open(Path(self.member_file), mode="r", encoding="utf-8") as members_file:
                for member_data in json.loads(members_file.read()):
                    self.add_member(PatchworkMember(**member_data))
                logger.info("Loaded Members")

    def groups_from_json_file(self):
        logger.info("Loading Groups from JSON")

        if not hasattr(self, "groups") or not bool(self.groups):
            with open(Path(self.groups_file), mode="r", encoding="utf-8") as groups_file:
                logger.info("Loaded Groups")
                self.groups = json.loads(groups_file.read())
        return self

    async def save(self, pg_pool: aiopg.Pool):
        await self.save_to_postgres(pg_pool)

    async def save_to_json(self):
        members = []
        groups = []
        for member in self.members:
            members.append(attr.asdict(member))
        for group in self.groups:
            groups.append(attr.asdict(group))

        async with aiofiles.open(Path(self.member_file), mode="w") as members_file:
            await members_file.write(json.dumps(members, indent=2, sort_keys=True))
        async with aiofiles.open(Path(self.groups_file), mode="w") as group_file:
            await group_file.write(json.dumps(groups, indent=2, sort_keys=True))

    async def save_to_postgres(self, pg_pool: aiopg.Pool) -> SystemModel:
        with await pg_pool.cursor() as cursor:
            try:
                await cursor.execute(
                    (
                        "INSERT INTO system_system(tupper_id, pluralkit_id, name, description, "
                        "tag, avatar_url, timezone, created_at, owner_discord_id)"
                        " VALUES(%s, %s, %s, %s, %s, %s, %s, CURRENT_TIMESTAMP, %s) RETURNING id"
                    ),
                    (
                        str(self.tupper_id) if self.tupper_id is not None else self.tupper_id,
                        str(self.pluralkit_id) if self.pluralkit_id is not None else self.pluralkit_id,
                        self.name,
                        self.description,
                        self.tag,
                        self.avatar_url,
                        self.timezone,
                        self.owner_discord_id,
                    ),
                )
            except Error:
                logger.info("IntegrityError Handled here-------")
                await cursor.execute(
                    (
                        "UPDATE system_system SET tupper_id=%s, pluralkit_id=%s, name=%s, description=%s,"
                        "tag=%s, avatar_url=%s, timezone=%s, created_at=CURRENT_TIMESTAMP, "
                        "owner_discord_id=%s WHERE owner_discord_id=%s RETURNING id;"
                    ),
                    (
                        str(self.tupper_id) if self.tupper_id is not None else self.tupper_id,
                        str(self.pluralkit_id) if self.pluralkit_id is not None else self.pluralkit_id,
                        self.name,
                        self.description,
                        self.tag,
                        self.avatar_url,
                        self.timezone,
                        self.owner_discord_id,
                        str(self.owner_discord_id),
                    ),
                )
            self.id_ = (await cursor.fetchone())[0]

        logger.info(f"Finished system {self.name} metadata save to database!")
        logger.info("Starting member saves")
        for member in self.members:
            await member.save_to_postgres(self.id_, pg_pool)
        logger.info("Finished member saves")
        return self

    def select_fronting_menu(self, ctx: tanjun.SlashContext):

        row = ctx.rest.build_action_row().add_select_menu("select_part").set_placeholder("Select a Member to Front")
        for member in self.members:
            row.add_option(member.name, member.id_).set_description(
                member.description[:30] if isinstance(member.description, str) else "-"
            ).add_to_menu()

        return row.add_to_container()

    def get_part_by_id(self, id_: int) -> PatchworkMember | None:
        for member in self.members:
            if int(member.id_) == id_:
                return member

        return None

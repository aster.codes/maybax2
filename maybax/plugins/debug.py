"""Debugger module only usable by bot owner."""
from __future__ import annotations

import datetime
import platform
import time

import hikari
import psutil
import tanjun
from tanjun.abc import SlashContext

component = tanjun.Component()

debug = component.with_slash_command(  # pylint: disable=C0103
    tanjun.slash_command_group("debug", "A bunch of debugger commands.")
)  # pylint: disable=C0103


@debug.with_command
@tanjun.as_slash_command("about", "About this bot.")
async def about_bot(ctx: SlashContext, process: psutil.Process = tanjun.cached_inject(psutil.Process)):
    """Get the bot's current delay."""
    bot_user = await ctx.rest.fetch_my_user()
    start_time = time.perf_counter()
    rest_latency = (time.perf_counter() - start_time) * 1_000
    gateway_latency = ctx.shards.heartbeat_latency * 1_000 if ctx.shards else float("NAN")
    start_date = datetime.datetime.fromtimestamp(process.create_time())
    uptime = datetime.datetime.now() - start_date
    memory_usage: float = process.memory_full_info().uss / 1024**2
    cpu_usage: float = process.cpu_percent() / psutil.cpu_count()
    memory_percent: float = process.memory_percent()
    help_links = """[Support Website & Editor](https://maybax.patchwork.systems/maybax-commands/)\n
    [Maybax Commands Documentation & Help](https://maybax.patchwork.systems/maybax-commands/)\n
    [Support Discord](https://discord.gg/SZcPc5WkSS)\n
    [The Patchwork Collective Patreon (Maybax Developers)](https://www.patreon.com/patchworkcollective)\n
    [Gitlab Home](https://gitlab.com/aster.codes/maybax2)\n
    [Report an Issue](https://gitlab.com/aster.codes/maybax2/-/issues)\n
    """
    patreon_supporters = """@Raekoam#2765"""
    embed = (
        hikari.Embed()
        .set_thumbnail(bot_user.make_avatar_url())
        .set_author(name=f"About {bot_user.username}", url="https://gitlab.com/aster.codes/maybax2/")
        .add_field(name="Uptime", value=f"{uptime}", inline=True)
        .add_field(
            name="Memory Usage",
            value=f"{memory_usage:.2f} MB ({memory_percent:.0f}%)",
            inline=True,
        )
        .add_field(name="CPU Usage", value=f"{cpu_usage:.2f}% CPU", inline=True)
        .add_field(name="REST Client Ping:", value=f"{rest_latency}", inline=True)
        .add_field(name="Gateway Client Ping:", value=f"{gateway_latency}", inline=True)
        .add_field("Help & Support", value=help_links, inline=False)
        .add_field("Thanks to our Patreon Supporters!", value=patreon_supporters, inline=False)
        .set_footer(
            text=f"Made with Tanjun and Python {platform.python_version()}",
        )
    )

    await ctx.respond(embed=embed)


@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    """Default tanjun loader"""
    client.add_component(component.copy())


@tanjun.as_unloader
def unload(client: tanjun.Client, /) -> None:
    client.remove_component_by_name(component.name)

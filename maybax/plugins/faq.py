"""Provides embeds giving information about frequently asked questions
on DID/OSDD"""
from __future__ import annotations

import datetime
from random import choice

import hikari
import tanjun
import yuyo
from tanjun.abc import SlashContext

TONE_INDICATOR_DICT = {
    "/srs": ["Serious", "I really like working with you /srs"],
    "/nsrs": ["Not Serious", "I just bruised my leg, but I'm okay. /nsrs"],
    "/p": ["Platonic", "I'd love to give you a hug. /p"],
    "/t": ["Teasing", "Oh really? Guess we have to be best friends now. /t"],
    "/nm": ["Not Mad", "Did you forget? /nm"],
    "/lu": ["Little Upset", "Our top client won't be renewing. /lu"],
    "/neg or /nc": ["Negative Connotation", "Oh my gosh. I'm going to cry.  /neg"],
    "/pos or /pc": ["Positive Connotation", "Oh my gosh! I'm going to cry.  /pos"],
    "/lh": ["Light Hearted", "I think you misspelled mosey. /lh"],
    "/nbh": ["Nobody Here", "I’m really feeling down because of someone. /nbh"],
    "/m": ["Metaphorically", "His fingers were icicles after shoveling for so long! /m"],
    "/li": ["Literally", "The rat we saw was the size of a small dog. /li"],
    "/ij": ["Inside Joke", "There’s just something about those beach fries. /ij"],
    "/j": ["Joke/Joking", "I’m basically the queen of Discord 👑 /j"],
    "/hj": ["Half Joking", "I mean, I’m usually right. /hj"],
    "/rh or /rt": ["Rhetorical Question", "I still can’t figure out the issue. How can I be so stupid? /rh"],
    "/gen or /g": ["Genuine (Not Sarcastic)", "You’re such a great person. /gen"],
    "/s": ["Sarcastic", "This is just what I needed on a Monday. /s"],
    "/hyp": ["Hyperbole", "I have 10,000 things to do today. /hyp"],
    "/pis": ["A mistake", "aloe: mmmm pizza /pis\n aloe: /pos*\naloe: i'm perishing now"],
}

TONE_IND_OPT = list(TONE_INDICATOR_DICT.keys())
TONE_IND_OPT.append("All")

faq = tanjun.slash_command_group("faq", "Learn about something!")  # pylint: disable=C0103


@faq.with_command
@tanjun.with_bool_slash_option("show", "Make visible to other people. (default False)", default=False)
@tanjun.with_str_slash_option(
    "indicator",
    "The indicator you want to look up (ex: /s) *Optional*.",
    choices=TONE_IND_OPT,
    default="All",
)
@tanjun.as_slash_command("tone", "Learn about tone indicators")
async def tone_indicators(
    ctx: SlashContext,
    indicator: str = "all",
    show: bool = False,
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
):
    """Provides an embed that explains tone indicators.

    Accepts a tone indcator to search. For example `/s`.

    Parameters
    ==========
    indicator: str
        The tone indicator to search by.
    """
    ctx.set_ephemeral_default(not show)
    description = (
        "Interpreting messages online can be difficult! "
        "Many people struggle to interpret more subtle meaning through text. "
        "\n\nTone indicators/tags aim to simplify that! \n\n"
        "A tone indicator is a symbol or shorthand word used "
        "to convey the tone of any text-based message. "
        "Tone indicators have become popular recently, but "
        "[has been around for a while]"
        "(https://en.wikipedia.org/wiki/Tone_indicator)."
        "\n\nLastly please remember 2 things! "
        "Please do not overuse tone indicators. This can feel very patronizing. "
        "Tone indicators don't reverse or negate. If you say something rude or mean, "
        "then add /npa, it's still passive aggressive. "
    )

    embed = (
        hikari.Embed(
            title="What are Tone Indicators?",
            description=description,
        )
        .set_author(name=ctx.member.username, icon=ctx.member.avatar_url)
        .set_footer(text="Submit more at https://gitlab.com/aster.codes/maybax2")
    )

    if guild := ctx.get_guild():
        embed.set_thumbnail(guild.icon_url)

    if indicator.lower() != "all":
        if found_indicator := TONE_INDICATOR_DICT.get(indicator.lower()):
            embed.add_field(
                name=indicator,
                value=found_indicator[0],
                inline=False,
            )
            embed.add_field(
                name="Example Usage",
                value=found_indicator[1],
                inline=False,
            )
    else:
        iterator = (
            (
                f"Viewing {num+1}/{len(TONE_INDICATOR_DICT)} Tone Indicators",
                (
                    hikari.Embed(
                        title="What are Tone Indicators?",
                        description=description,
                    )
                    .set_author(name=ctx.member.username, icon=ctx.member.avatar_url)
                    .set_footer(text="Submit more at https://gitlab.com/aster.codes/maybax2")
                    .add_field(
                        name=indicator,
                        value=f"{TONE_INDICATOR_DICT[indicator][0]}\nExample: {TONE_INDICATOR_DICT[indicator][1]}\n\n",
                        inline=False,
                    )
                ),
            )
            for num, indicator in enumerate(TONE_INDICATOR_DICT)
        )
        paginator = yuyo.ComponentPaginator(iterator, authors=(ctx.author,), timeout=datetime.timedelta(minutes=3))

        if first_response := await paginator.get_next_entry():
            content, embed = first_response
            message = await ctx.edit_initial_response(content=content, component=paginator, embed=embed)
            component_client.set_executor(message, paginator)
            return

    await ctx.respond(embed=embed)


osdd = faq.with_command(tanjun.slash_command_group("osdd", "Learn about DID/OSDD!"))  # pylint: disable=C0103


@osdd.with_command
@tanjun.with_bool_slash_option("show", "Make visible to other people. (default False)", default=False)
@tanjun.as_slash_command("what-is", "Learn what DID/OSDD is!")
async def what_is_osdd(ctx: SlashContext, show: bool = False) -> None:
    """Provides a command written by the Patchwork Collective that
    explains how DID/OSDD is."""
    colors = [1842204, 16428121, 16777215]
    color = choice(colors)  # noqa: S311
    guild = ctx.get_guild()
    description = (
        f"Welcome to {guild.name}! \nIt looks like "
        "you're curious about DID and OSDD! We aim to be as "
        "inclusive as possible, so we have a number of great "
        "resources here to help. Below are a links that might "
        "help!"
    )
    embed = hikari.Embed(title="What is DID/OSDD", description=description, colour=color)
    embed.set_thumbnail(
        "https://cdn.discordapp.com/attachments/1049108212663726080/1049415528646725773/IMG_2918_JPG.png"
    )

    embed.add_field(
        name="Patchwork Collectives Intro to OSDD Series",
        value="The Patchwork Collective wrote a short series "
        "going over a what exactly [OSDD is.]"
        "(https://patchwork.systems/osdd/what-is-osdd.html)",
        inline=False,
    )
    embed.add_field(
        name="SciShow Video on Dissociative Identity Disorder:",
        value=(
            "Hank Green presents a [5 minute video]"
            "(https://www.youtube.com/watch?v=l4hVtBV5o4s) going over "
            "the basics of Dissociative Identity Disorders and how the "
            "media misportrays these disorders."
        ),
        inline=False,
    )
    embed.add_field(
        name="D-I-D You Know Graphic",
        value=(
            "tigrin on DeviantArt provides a [great explanation of"
            " Dissociative Disorders in an infographic comic]"
            "(https://www.deviantart.com/tigrin/art/D-I-D-You-Know-58072489)."
            " They go over what DID is like and how Hollywood portrays "
            "it incorrectly."
        ),
        inline=False,
    )
    embed.add_field(
        name="BetterTogether's DID/OSDD Casually and Visually Explained",
        value=(
            "BetterTogether's tumblr offers many great DID/OSDD "
            "resources, including their wonderful "
            "[DID/OSDD Casually Explained]"
            "(https://clever-and-unique-name.tumblr.com/post/187704319719/didosdd-casually-explained-masterpost)."
            " Great explanations in a very simple to understand visual "
            "style!"
        ),
        inline=False,
    )
    ctx.set_ephemeral_default(not show)
    await ctx.respond(embed=embed)


TALKING_BOTS = (
    "These `[BOT]` messages are commonly called 'proxies' or 'Proxied Messages' "
    "and are created using bots like [TupperBox](https://tupperbox.app/), "
    "[Pluralkit](https://pluralkit.me/) or [Myself](https://maybax.patchwork.systems/)!\n"
)


@osdd.with_command
@tanjun.with_bool_slash_option("show", "Make visible to other people. (default False)", default=False)
@tanjun.as_slash_command("talking-bots", "Why does it look like the [BOT] accounts are talking?")
async def talking_bots(ctx: SlashContext, show: bool = False) -> None:
    colors = [1842204, 16428121, 16777215]
    color = choice(colors)  # noqa: S311
    embed = (
        hikari.Embed(title="Why are accounts with a 'BOT' tag talking?", description=TALKING_BOTS, colour=color)
        .add_field(
            name="Why use Proxy Messages?",
            value=(
                "Proxy Messages help people with DID/OSDD to communicate more easily. "
                "Proxies allow Parts in a System to have different pfps, usernames and "
                "be more easily identifiable. This helps not only the person with DID/OSDD,"
                " but also to moderators and other members. "
                "The quick deleting can be annoying or off-putting and we apologize for that. "
                "But this greatly helps accommodate members with disabilities."
            ),
            inline=False,
        )
        .add_field(
            name="What is DID/OSDD?",
            value=(
                "[DID/OSDD](https://en.wikipedia.org/wiki/Dissociative_identity_disorder) is estimated to affect"
                " between 1.5-3% of the population in NA and the EU. "
                "There are several commands that provide brief overviews of the disorder:"
                "```/faq osdd <what-is / split / switching / manners>```\n"
                " If you are new to this topic we recommend starting with:\n"
                "```/faq osdd what-is``` Gives an intro to DID/OSDD."
                " If you have any questions, we are generally happy to help educate. Just please make sure to "
                "ask respectfully and read some of the information first (especially `/faq osdd manners`!)."
            ),
            inline=False,
        )
        .add_field(
            name="Be Respectful",
            value="Please remember to be respectful to all members regardless of any disabilities.",
            inline=False,
        )
    )
    ctx.set_ephemeral_default(not show)
    await ctx.respond(embed=embed)


@osdd.with_command
@tanjun.with_bool_slash_option("show", "Make visible to other people. (default False)", default=False)
@tanjun.as_slash_command("parts-affecting", "Learn how Parts/Alters affect each other!")
async def parts_affecting(ctx: SlashContext, show: bool = False):
    """Provides a command written by the Patchwork Collective that
    explains how DID/OSDD Parts/Alters affect each other."""
    colors = [16412539, 5798125, 16763174]
    color = choice(colors)  # noqa: S311
    embed = hikari.Embed(
        title="How Alters/Parts Affect Each Other",
        description=(
            "The experiences described below are informally or loosely defined terms "
            "for common DID/OSDD system experiences. These terms/experiences may not "
            "be applicable to every system. Every system may not experience these "
            "terms in the same way. Some systems may even have additional experiences"
            " not listed."
        ),
        colour=color,
    )
    embed.set_thumbnail(
        "https://cdn.discordapp.com/attachments/8386132"
        "30536359997/863454018545516605/70351780a934458"
        "9c41b28b2bba621f1aaa8b2e0.png"
    )
    embed.add_field(
        name="Patchwork Collective's Understanding Parts - Substitute Belief and why Parts look the way they do.",
        value=(
            "This article goes into detail about [Substitute Beliefs]"
            "(https://patchwork.systems/osdd/understanding-parts.html)."
            "Understanding SB helps explain why Parts believe the things "
            "they do. \n\n"
            "Information taken for this article is sourced and noted in the footnotes."
            "The primary source of this article is"
            "The Haunted Self: Structural Dissociation and the Treatment of Chronic "
            "Traumatization (Norton Series on Interpersonal Neurobiology)."
        ),
        inline=False,
    )
    embed.add_field(
        name="Patchwork Collective's articles - Switching, Blurry, & SubSystems",
        value=(
            "Patchwork offers several other articles that help explain "
            "how System Parts interact. [Switching]"
            "(https://patchwork.systems/osdd/switching.html)"
            " looks at how Parts switch and what causes it. "
            "[Blurry](https://patchwork.systems/osdd/blurry.html)"
            " discusses what happens when Part's blend together. "
            "Lastly, [Subsystems](https://patchwork.systems/osdd/subsystems.html)"
            " explains how Parts group inside of a System and how that "
            "grouping affects them!"
        ),
        inline=False,
    )
    embed.add_field(
        name="BetterTogether's How Alters/Parts Are Experienced Internally",
        value=(
            "This tumblr artist [provides a wonderful visual explanation]"
            "(https://clever-and-unique-name.tumblr.com/po"
            "st/185276103179/thank-you-dissociatingdingo-for-being-my-editor)"
            " of how Alters commonly experience other "
            "Alters internally. "
            "The infographic covers idea's like "
            '"passive influence," "dormancy" and more!'
        ),
        inline=False,
    )
    embed.add_field(
        name="BetterTogether's Parts of Self",
        value=(
            "Alters perception of how their consciousness "
            "exists with other alters consciousness (in "
            "their system) can fluctuate. This perception"
            " can change multiple times an hour or stay "
            "consistent for long periods. [BetterTogether"
            " provides a great infographic]"
            "(https://clever-and-unique-name.tumblr.com/post/185228806634/since-im-still-waiting-for-replacement-pen-tips)"
            " that visually explains a few of these states, "
            "including CoCon'ing and Blurring/Blurry."
        ),
        inline=False,
    )
    embed.add_field(
        name="BetterTogether's One Person's Experience Having Dissociative Parts",
        value=(
            "[BetterTogether provides their personal "
            "experience having Dissociative Parts.]"
            "(https://clever-and-unique-name.tumblr.com/post/641471968521109504/i-made-a-thing-thats-entirely-based-off-personal)"
            " It is a wonderful piece that covers many"
            " common questions."
        ),
        inline=False,
    )
    ctx.set_ephemeral_default(not show)
    await ctx.respond(embed=embed)


@osdd.with_command
@tanjun.with_bool_slash_option("show", "Make visible to other people. (default False)", default=False)
@tanjun.as_slash_command("split", "Learn about Splitting")
async def split(ctx: SlashContext, show: bool = False) -> None:
    """Provides a command written by the Patchwork Collective that
    explains DID/OSDD switching."""
    embed = hikari.Embed(title="Splitting")
    embed.set_thumbnail(
        "https://64.media.tumblr.com/f6f2c5de5188d8b"
        "a4c1187cdafc4ce09/0b272e3ec895fac3-d4/s400x"
        "600/de76a995c7e8e0a6cf3b054ec8f452a5747db84f.png"
    )
    embed.add_field(
        name="The Entropy System - Splitting - When a New Identity Forms",
        value=(
            "EntropySystem makes loads of wonderful "
            "DID/OSDD awareness videos on Youtube! "
            "In [this video they explain what splitting "
            "means](https://www.youtube.com/watch?v=AgB29CCcGO4)"
            ", how it affects Systems, and recounts"
            " a personal experience."
        ),
        inline=False,
    )
    embed.add_field(
        name="BetterTogether's Splits in Established Systems",
        value=(
            "[This infographic explains what splitting is]"
            "(https://clever-and-unique-name.tumblr.com/post/189763970544/there-is-no-logical-or-natural-limit-to-how)"
            " and why it can occur in Systems that seem "
            'to be "doing fine".'
        ),
        inline=False,
    )
    ctx.set_ephemeral_default(not show)
    await ctx.respond(embed=embed)


@osdd.with_command
@tanjun.with_bool_slash_option("show", "Make visible to other people. (default False)", default=False)
@tanjun.as_slash_command("switch", "Learn about Switching")
async def switching(ctx: SlashContext, show: bool = False) -> None:
    """Explains what switching is!"""
    colors = [16758273, 2863491]
    color = choice(colors)  # noqa: S311
    embed = hikari.Embed(
        title="Switching",
        description=(
            "Switching is when one Part/Alter exchanges control "
            "of parts or all of the body with another Alter. "
            'This could be as small as letting one Part "Switching"'
            "into a body part as small as a finger, or even the whole"
            "/entire body!\n\n"
            "Switching is not always on purpose! Some Parts can "
            "force switches or prevent them, like GateKeepers."
        ),
        color=color,
    )
    embed.add_field(
        name='"Switchy"',
        value=(
            "A feeling that many Systems experience. It "
            "describes feeling like an Alter is close to "
            "switching but is not. It can also mean feeling "
            "an Alter is trying to force a switch."
        ),
        inline=False,
    )
    embed.set_thumbnail(
        "https://64.media.tumblr.com/17faccb62cd439db26"
        "300cdf333cbf49/e64c806c1fdb7a70-00/s400x600/56d"
        "02eaa0db306afef458305df12c7aee5273979.png"
    )
    ctx.set_ephemeral_default(not show)
    await ctx.respond(embed=embed)


OSDD_MANNERS = {
    "Alters are not the same person, please do not treat them as such.": (
        "DID/OSDD is a very complex subject, even more so in regards"
        " to the Parts/Alters and memories. Many Parts will act strikingly "
        "different. Topics or conversations you have had with one Part may "
        "not be known or remembered by another. Alters also have vastly different "
        "skillsets. Some parts may be able to program, while others are more "
        "socially skilled, while yet others only purpose is self-care."
    ),
    "Asking for other Alters.": (
        "In general it is disrespectful to ask for"
        " another Alter/Part. The Part fronting can offer "
        "but it is very rude to assume or ask. "
        "Switching is not always possible to do on a whim."
        "\n\n"
        "This is a general rule and some Systems may not mind."
        " It is better to ask first."
    ),
    "Positive/Negative triggering an alter out without permission.": (
        "Most Parts will have specific triggers that will force "
        "that part either to or away from the front. It is considered "
        "very disrespectful to purposefully trigger a Part. "
        "Triggering, both positive and negative, is very draining "
        "and can be extremely disruptive to the Systems daily life."
    ),
    "This is not a trend or a fun thing.": (
        "Many social media sites such as Twitter and TikTok "
        "have massively magnified self diagnosis and a new "
        "'Plurality' trend. DID/OSDD is not this.\n\n"
        "DID and OSDD are both DSM-V recognized disorders "
        "that required a trained clinical psychitrist to diagnosis."
        " In this bot developers experience, it requires over "
        "18 months (1.5 years) of weekly therapy sessions to "
        "get such a diagnosis.\n\n"
        "It is understandable to be skeptical at first, but please "
        "remember this is a disability that is out of the persons control. "
        "No matter how disruptive and annoying *you* are finding "
        "the disorder to deal with, rest assured the disordered person "
        "is feeling it 1,000x more."
    ),
    "Speaking on DID/OSDD in a non-personal way.": (
        "While some people may find DID/OSDD to be an interesting "
        "subject to discuss, it can be uncomfortable to have people "
        "talk about a disorder members have. This kind of conversation "
        "can often come off as being very cold and making the disordered "
        "person feel like a science project. This is a very unwelcoming "
        "environment. Asking the person first and asking clarifying "
        "questions is a much kinder way to interact."
    ),
    "Drawing excessive negative attention to disordered members.": (
        "Members with developmental (Autism) or other disorders "
        "often tend to be a target for negative attention. It should"
        " not have to be said, but please do not do this. Mocking "
        "someone for using accommodation tools like tone indicators "
        "or proxy messages is ablist. This would be similar to mocking "
        "a person with a wheel chair or crutches. Just treat all members "
        "kindly and equally."
    ),
}


@osdd.with_command
@tanjun.with_bool_slash_option("show", "Make visible to other people. (default False)", default=False)
@tanjun.as_slash_command("manners", "Learn about Manners with Systems")
async def manners(
    ctx: SlashContext,
    show: bool = False,
    component_client: yuyo.ComponentClient = tanjun.injected(type=yuyo.ComponentClient),
) -> None:
    """
    Some behavior can be very rude and uncomfortable for Systems. If you're not familiar, read this!
    """
    ctx.set_ephemeral_default(not show)
    colors = [16758273, 2863491]
    color = choice(colors)  # noqa: S311
    iterator = (
        (
            f"{num+1}/{len(OSDD_MANNERS)}",
            (
                hikari.Embed(
                    title="Courtesy with Systems",
                    description=(""),
                    color=color,
                )
                .set_author(name=ctx.member.username, icon=ctx.member.avatar_url)
                .set_footer(text="Submit more at https://gitlab.com/aster.codes/maybax2")
                .set_thumbnail(
                    "https://64.media.tumblr.com/91549526d7d6eb4671b5149fbd2ba607/"
                    "7ec43f85249903a1-51/s500x750/ec0304f0a30195e474610f5c6d09bd0a8873255a.png"
                )
                .add_field(
                    name=manner,
                    value=OSDD_MANNERS[manner],
                    inline=False,
                )
            ),
        )
        for num, manner in enumerate(OSDD_MANNERS)
    )
    paginator = yuyo.ComponentPaginator(iterator, authors=(ctx.author,), timeout=datetime.timedelta(minutes=3))

    if first_response := await paginator.get_next_entry():
        content, embed = first_response
        message = await ctx.edit_initial_response(content=content, component=paginator, embed=embed)
        component_client.set_executor(message, paginator)
        return


autism = faq.with_command(  # pylint: disable=C0103
    tanjun.slash_command_group("autism-spectrum-disorder", "Learn about ASD/Autism Spectrum Disorder!")
)


ASD_TERMS = {
    "comorbid": (
        'Oxford defines this as: "denoting or relating to diseases or medical '
        'conditions that are simultaneously present in a patient." ASD is often '
        "comorbid with OCD, SPD, Epilepsy, Gastro Disorders, Anxiety, Depression, and BPD"
    ),
    "special interest": (
        "Individuals with ASD often take one or more Special Interests (SI). "
        "These SI often manifest in intense and passion interests in a subject. "
        "The individual may lose themself in the topic to the degree they forget "
        "to eat, handle responsibilities, sleep, or even not noticing physical "
        "pain and further damaging the body."
        "\n"
        'While sometimes compared to an "intense nt hobby," SI\'s are often a '
        "requirement for individuals with ASD."
    ),
    "infodumping": (
        "Also called monologuing, is when an individual with ASD talks at length, "
        "usually about an SI. This can often come with a lack of awareness of other "
        "parties interest via body language and subtle reactions. [Keira Fulton-Lees]"
        "(https://medium.com/illumination-curated/the-autistic-trait-that-everyone-hates-1a4c725a0582)"
        " has a great post about InfoDumping on Medium."
    ),
    "meltdown": (
        "If an individual with ASD becomes overwhelmed by stress or a situation, may result"
        " in a meltdown. Meltdowns are entirely out of the individuals control and are no caused"
        " by anger or a temper tantrum as some imply. This loss of control may manifest in "
        "verbal outbursts like shouting, screaming and crying; physical outbursts like kicking, "
        "lashing out, or biting; or both."
    ),
    "shutdown": (
        "Inversely to a Meltdown which expresses and regulates emotions externally, a shutdown does"
        " so internally. Shutdowns are also caused by over stimulation or becoming overwhelmed. Shutdowns "
        "result in the individual going totally or mostly unresponsive. Just like Shutdowns, this is "
        "totally out of the individuals control."
    ),
    "echolalia": (
        "Echolalia causes individuals to repeat noises, sounds or phrases that they hear. ASD individuals "
        "can often develop Echolalia with verbal stimming as a self regulation method. Echolalia is very "
        "different from Tourettes Syndrome, which causes a person to to suddenly say or yell random "
        "things suddenly as a part of a tic."
    ),
    "stim": (
        "Self-Stimulatory behavior consists of repetitive actions or "
        "movements meant to help self regulate emotion "
        " or help alleviate stress. Stimming comes in "
        "many forms. It can be physical like spinning in circles, "
        '"hand flapping," or rocking. Stimming can also'
        " be vocal, the individual may repeat a noise over and over "
        "simply because it is a pleasing noise. \n\nMost stimming "
        "behavior is natural and out of the persons control."
    ),
    "spd": (
        "Wikipedia describes SPD as a condition in which multisensory "
        "input is not adequately processed in order to provide appropriate "
        "responses to the demands of the environment.\n\n"
        "This response can be hypoactive or hyperactive. One person with "
        "SPD may crave a specific tactile feeling while another "
        "will go to extremes to avoid it."
    ),
    "nt": (
        'Oxford defines Neurotypical (NT) as "not displaying '
        "or characterized by autistic or other neurologically atypical patterns"
        'of thought or behavior." NT is a term widely used in '
        "the ASD community as a label for someone without ASD or any other"
        " neurological or mental disorders."
    ),
    "dyspraxia": (
        "Dyspraxia, also known as developmental coordination disorder (DCD),"
        " is a developmental disorder that causes problems with movement, "
        "coordination, judgment, processing, memory, and some other cognitive "
        "skills. It can also affect the body’s immune and nervous systems. "
        "Dyspraxia is fairly common and a very frequent comorbidity of Autism."
    ),
    "efd": (
        "Executive functions are a broad group of skills that enable people "
        "to complete tasks and interact with others. An Executive Function "
        "Disorder (EFD) can impair a person's ability to organize themselves "
        "and control their behavior--these include ADHD and ADD, though "
        "someone can be given a broad diagnosis of EFD."
    ),
    "ocd": (
        "Frequently misrepresented in media, Obsessive Compulsive Disorder is "
        "a disorder in which people have recurring, unwanted thoughts, ideas, "
        "or sensations (obsessions) that make them feel driven to do something "
        "repetitively (compulsions). This can range from worries about negative "
        "reactions from deviating from rigid routines to violent or otherwise "
        "unsavory intrusive thoughts or compulsions."
    ),
    "masking": (
        "Masking is a complex and costly survival strategy for Autistic people. "
        "It generally involves intentionally learning neurotypical behaviors and "
        "mimicking them in social situations. Though often learned naturally from "
        "a young age, this behavior can be incredibly damaging to Autistic people "
        "as their lives go on."
    ),
    "rsd": (
        "Rejection Sensitive Dysphoria (RSD) is a highly common symptom of EFD, "
        "specifically ADHD. This is generally a strong emotional reaction to "
        "perceived rejection, which can range from angry lashing out to outright "
        "panic. People who have the condition sometimes work hard to make everyone "
        "like and admire them, or they might stop trying and stay out of any "
        "situation where they might get hurt. "
    ),
}
ASD_TERMS_OPT = list(ASD_TERMS.keys())
ASD_TERMS_OPT.append("All")


@autism.with_command
@tanjun.with_bool_slash_option("show", "Make visible to other people. (default False)", default=False)
@tanjun.with_str_slash_option(
    "term",
    "The term you want to look up (ex: meltdown) *Optional*.",
    choices=ASD_TERMS_OPT,
)
@tanjun.as_slash_command("terminology", "Learn some terminology common to Autism Spectrum Disorder (ASD)")
async def asd_terminology(ctx: SlashContext, term: str = "all", show: bool = False) -> None:
    """Provides a search/filterable slash command about ASD terminology."""
    return_terms: dict = ASD_TERMS
    if term.lower() != "all":
        found_terms = ASD_TERMS.get(term.lower(), ASD_TERMS)

        if found_terms:
            return_terms = {term: found_terms}

    description = ""
    embed = hikari.Embed(title="Autism Spectrum Terminology", description=description)
    for found_term, found_def in return_terms.items():
        embed.add_field(name=found_term, value=found_def, inline=False)
    if guild := ctx.get_guild():
        embed.set_thumbnail(guild.icon_url)

    ctx.set_ephemeral_default(not show)
    await ctx.respond(embed=embed)


component = tanjun.Component().load_from_scope().make_loader()

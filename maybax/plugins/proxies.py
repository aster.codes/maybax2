from __future__ import annotations

import logging
import os

import aiopg
import aioredis
import hikari
import tanjun
from hikari import GuildMessageCreateEvent, IncomingWebhook
from models.front.members import PatchworkMember
from models.front.system import SystemModel

ALLOWED_PROXY_KEY = "{guild_id}:allowed_proxy_role_id"


component = tanjun.Component()
logger = logging.getLogger(__name__)


@component.with_listener(hikari.GuildMessageCreateEvent)
async def proxy_message_event(
    event: hikari.GuildMessageCreateEvent,
    system_model: SystemModel = tanjun.injected(type=SystemModel),
    pg_pool: aiopg.Pool = tanjun.injected(type=aiopg.Pool),
    redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis),
):
    """Listener for message send to start proxy."""
    system_model = await system_model.setup(event.author.id, pg_pool, redis)
    guild_key = ALLOWED_PROXY_KEY.format(guild_id=event.guild_id)
    proxy_role_id = await redis.get(guild_key) if (await redis.get(guild_key) is not None) else 0

    if (
        not system_model
        or not event.member
        or event.member.is_bot
        or int(proxy_role_id) not in set(event.member.role_ids)
        or event.content is None
    ):
        return

    member, matched_tag = process_message(event.content, system_model=system_model)

    message_content = ""
    if not member and system_model.auto and system_model.fronting:
        message_content = event.content if event.content is not None else "-"
    elif matched_tag:
        system_model.fronting = member
        await system_model.cache(event.member.id, redis)  # Cache the newly fronting member
        tagless_message = event.content.split(matched_tag.strip())
        if tagless_message[0]:
            message_content = tagless_message[0]
        else:
            message_content = tagless_message[1]
    else:
        return

    await convert_message_to_webhook(event, system_model.fronting, message_content, system_model=system_model)


def process_message(event: str, system_model: SystemModel) -> list[PatchworkMember | str | None]:
    """Help function to check if an incoming message matches a possible proxy member."""
    for member in system_model.members:
        if member.indicators:
            for indicator in member.indicators:
                if (
                    indicator.get("suffix", None)
                    and indicator.get("suffix", "").strip() != ""
                    and event.endswith(indicator.get("suffix", "").strip())
                ):
                    return [
                        member,
                        indicator["suffix"],
                    ]
                if (
                    indicator.get("prefix", None)
                    and indicator.get("prefix", "").strip() != ""
                    and event.startswith(indicator.get("prefix", "").strip())
                ):
                    return [
                        member,
                        indicator["prefix"],
                    ]
    return [
        None,
        None,
    ]


async def convert_message_to_webhook(
    event: GuildMessageCreateEvent,
    member: PatchworkMember,
    message: hikari.UndefinedOr[str],
    system_model: SystemModel,
):
    """
    Helper function to send a proxy a message via webhook.

    Currently supports:
    - pfp
    - member name
    - auto add | Patchwork Collective
    - message
    - replies
    - embed_message
    - attachments
    """
    tag = ""
    if member.tag:
        tag = member.tag
    elif system_model.tag:
        tag = system_model.tag
    data = {
        "webhook": None,
        "token": None,
        "content": message,
        "avatar_url": member.avatar_url,
        "username": f"{member.name}{tag}",
    }

    webhook = await get_webhooks(event)
    if not webhook:
        webhook = await create_webhook(event)

    data["webhook"] = webhook
    data["token"] = webhook.token

    if event.message.referenced_message:
        data["reply"] = event.message.referenced_message

    if event.message.attachments:
        attachments = []
        for attachment in event.message.attachments:
            attachments.append(await attachment.read())
        data["attachments"] = attachments

    await event.app.rest.execute_webhook(**data)
    await event.message.delete()


async def get_webhooks(
    event: hikari.GuildMessageCreateEvent,
) -> hikari.PartialWebhook | None:
    """Helper function to a webhook from the channel if it exists."""
    bot_username = os.environ.get("BOT_NAME", "Maybax")
    webhooks = await event.app.rest.fetch_guild_webhooks(event.guild_id)

    for webhook in webhooks:
        if webhook.channel_id == event.channel_id and f"{bot_username}-Proxy" == webhook.name:
            return webhook

    return None


async def create_webhook(
    event: hikari.GuildMessageCreateEvent,
) -> IncomingWebhook:
    """Helper function to create a webhook with the
    bots name."""
    bot_username = os.environ.get("BOT_NAME", "Maybax")

    webhook = await event.app.rest.create_webhook(
        channel=event.channel_id,
        name=f"{bot_username}-Proxy",
        reason="Webook for Maybax Proxying!",
    )

    return webhook


@tanjun.as_loader
def load(client: tanjun.abc.Client) -> None:
    """Default tanjun loader"""
    client.add_component(component.copy())


@tanjun.as_unloader
def unload(client: tanjun.Client, /) -> None:
    client.remove_component_by_name(component.name)

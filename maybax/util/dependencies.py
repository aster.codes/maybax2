"""Dependency Injectors for Maybax"""
from __future__ import annotations

import logging
import os

import aiopg
import aioredis
import httpx
import tanjun
from models.front.system import SystemModel

logger = logging.getLogger(__name__)


class HttpxInjector:
    """
    Dependency injector for httpx with tanjun.

    Register dependency:
        client.set_type_dependency(httpx.AsyncClient, HttpxInjector())

    Then get in plugin:

    async def func_name(redis: httpx.AsyncClient = tanjun.injected(type=httpx.AsyncClient))
    """

    def spawn_client(self) -> httpx.AsyncClient:
        logging.info("New httpx Client Spawned")
        return httpx.AsyncClient()


class RedisPoolDependency:
    """
    Dependency injector for aioredis with tanjun.

    Register dependency:
        client.set_type_dependency(aioredis.Redis, RedisPoolDependency())

    Then get in plugin:

    async def func_name(redis: aioredis.Redis = tanjun.injected(type=aioredis.Redis))
    """

    __slot__ = ("_redis_pool",)

    def __init__(self) -> None:
        host = os.environ.get("REDIS_HOST", "localhost")
        self._redis_pool = aioredis.ConnectionPool.from_url(
            f"redis://{host}", max_connections=10, decode_responses=True
        )

    def setup_connection(self):
        return aioredis.Redis(connection_pool=self._redis_pool)


class PostgresPoolDependency:
    """
    Dependency injector for aiopg with tanjun.

    Register dependency:
        client.set_type_dependency(aiopg.Pool, PostgresPoolDependency())

    Then get in plugin:

    async def func_name(redis: aiopg.Pool = tanjun.injected(type=aiopg.Pool))
    """

    __slot__ = ("_postgres_pool",)

    def __init__(self) -> None:
        self._postgres_pool = None

    def load_into_client(self, client: tanjun.Client) -> PostgresPoolDependency:
        client.add_client_callback(tanjun.ClientCallbackNames.STARTING, self.open).add_client_callback(
            tanjun.ClientCallbackNames.CLOSED, self.close
        )
        return self

    async def open(self, client: tanjun.Client = tanjun.injected(type=tanjun.Client)) -> None:
        if self._postgres_pool:
            raise RuntimeError("Session already running")

        HOST = os.environ.get("PG_HOST", "localhost")
        DB_NAME = os.environ.get("PG_NAME", "maybax")
        USER = os.environ.get("PG_USER", "jeremytiki")
        PASS = os.environ.get("PG_PASS", "Zelda!23")

        self._postgres_pool = await aiopg.create_pool(
            f"dbname={DB_NAME} user={USER} password={PASS} port=5432 host={HOST}"
        )

        client.set_type_dependency(aiopg.Pool, self._postgres_pool)

    async def close(self, client: tanjun.Client = tanjun.injected(type=tanjun.Client)) -> None:
        if not self._postgres_pool:
            raise RuntimeError("Session not running")

        pool: aiopg.Pool = self._postgres_pool
        await pool.close()
        self._postgres_pool = None
        client.remove_type_dependency(aiopg.Pool)

    def __call__(self):
        return self._postgres_pool


class PatchworkDep:
    """
    Dependency injector for PatchworkDep with tanjun.

    Register dependency:
        client.set_type_dependency(PatchworkDep, PatchworkDep)

    Then get in plugin:

    async def func_name(redis: PatchworkDep = tanjun.injected(type=PatchworkDep))

    """

    _instance = None
    _member_file: str = "./data/patchwork-collective.json"
    _groups_file: str = "./data/patchwork-collective--groups.json"

    def __new__(cls):
        if cls._instance is None:
            logger.info("PatchworkDep __new__ _instance created.")
            cls._instance = super().__new__(cls)
            cls._instance.system_model = SystemModel
            return cls._instance

    @classmethod
    def setup(cls) -> SystemModel:
        return cls()._instance.system_model

    def __call__(self, member_id: int) -> SystemModel:
        logger.info("PatchworkDep called")
        return self._instance.system_model

import datetime
import json
import logging

logger = logging.getLogger(__name__)


def asdict_export_serializer(instance, field_name, field_value):
    if field_value == "":
        return None
    return field_value


def attrs_asdict_to_redis_serializer(instance, field_name, field_value):
    if isinstance(field_value, type(None)):
        return ""
    elif isinstance(field_value, bool):
        return 1 if field_value else 0
    elif isinstance(field_value, datetime.datetime):
        return datetime.datetime.isoformat(field_value)
    elif isinstance(field_value, list) and field_name.name == "indicators":
        indicators = []
        for ind in field_value:
            if not ind["prefix"]:
                ind["prefix"] = "[NONE]"
            if not ind["suffix"]:
                ind["suffix"] = "[NONE]"
            indicators.append(f"prefix;{ind['prefix'].strip()},suffix;{ind['suffix'].strip()}")
        return "|".join(indicators)
    elif field_name.name == "fronting":
        pass

    return field_value


def attrs_to_postgres_serializer(instance, field_name, field_value):
    if field_name.name == "indicators":
        return json.dumps(field_value)
    elif field_name.name == "birthday":
        if field_value == "":
            return None
    return field_value


def attrs_postgres_exclude_filter(field_name, field_value):
    return field_name.name not in [
        "id_",
    ]

from __future__ import annotations

import logging

import hikari
from tanjun import ConversionError, ParserError
from tanjun.abc import Context

logger = logging.getLogger(__name__)


async def on_error(ctx: Context, exception: BaseException) -> None:
    title = f"An unexpected {type(exception).__name__} occurred"
    embed = hikari.Embed(
        title=title,
        colour=16711680,
        description=f"```python\n{str(exception)[:1950]}```",
    )
    await ctx.respond(embed=embed)
    logger.error(f"{title}: \n\n {str(exception)}")


async def on_parse_error(ctx: Context, exception: ParserError):
    message = str(exception)

    if isinstance(exception, ConversionError) and exception.errors:
        if len(exception.errors) > 1:
            message += ":\n* " + "\n* ".join(map("`{}`".format, exception.errors))
        else:
            message = f"{message}: `{exception.errors[0]}`"

    await ctx.respond(content=message)
    logger.error(message)

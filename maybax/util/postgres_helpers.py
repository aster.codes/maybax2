from __future__ import annotations

import logging

import aiopg
import hikari
from psycopg2.extras import RealDictCursor

logger = logging.getLogger(__name__)


async def ensure_guild_settings_exist(guild_id: hikari.Snowflakeish, pg_pool: aiopg.Pool) -> int | None:
    guild_id_str = str(guild_id)
    with await pg_pool.cursor(cursor_factory=RealDictCursor) as cursor:
        try:

            """INSERT INTO guildownersettings_guildownersettings (guild_id)
            SELECT %s
            WHERE NOT EXISTS (
                SELECT id, guild_id
                FROM guildownersettings_guildownersettings
                WHERE guild_id = %s
            )
            returning id, guild_id""",
            await cursor.execute(
                """
                INSERT INTO guildownersettings_guildownersettings (guild_id)
                VALUES (%s )
                ON CONFLICT (guild_id)
                DO NOTHING RETURNING id;
                """,
                (guild_id_str,),
            )
            result = await cursor.fetchone()

            if not result:
                await cursor.execute(
                    "SELECT id FROM guildownersettings_guildownersettings WHERE guild_id=%s LIMIT 1;", (guild_id,)
                )
                result = await cursor.fetchone()

            return result["id"]
        except Exception as e:
            logger.error(e)
            return None


async def set_guild_proxy_role(guild_db_id: int, role_id: int, pg_pool: aiopg.Pool) -> bool:
    with await pg_pool.cursor(cursor_factory=RealDictCursor) as cursor:
        try:
            await cursor.execute(
                """INSERT INTO system_guildsystemsettings (guild_id, system_role)
                VALUES (%s, %s)
                ON CONFLICT ON CONSTRAINT system_guildsystemsettings_guild_id_key
                DO
                    UPDATE SET system_role=EXCLUDED.system_role;""",
                (guild_db_id, role_id),
            )
            return True
        except Exception as e:
            logger.error(e)
            return False

image: "python:3.9.5"

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  key:
    files:
      - .gitlab-ci.yml
    prefix: ${CI_JOB_NAME}
  paths:
    - .venv
    - .cache/pip

before_script:
  - apt-get update -qq
  - apt-get install -qq git
  - pip install --upgrade pip
  - pip install nox
  - pip install pip-tools

stages:
  - Code Formatting
  - Linting
  - Testing
  - Deploy to Server

Black and iSort Format:
  stage: Code Formatting
  script:
    - nox -s reformat_code

SpellCode Spell Check:
  stage: Linting
  script:
    - nox -s spell_check

PyTest:
  stage: Testing
  allow_failure: true
  script:
    - nox -s pytest_code
  artifacts:
    paths:
      - ./html_coverage_report/

PyLint Lint:
  stage: Linting
  allow_failure: true
  script:
    - pip install pylint-exit
    - rm -rf pylint; mkdir pylint
    - poetry run pylint maybax | tee ./pylint/pylint.log || pylint-exit $?
    - PYLINT_SCORE=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' ./pylint/pylint.log)
    - poetry run anybadge --label=Pylint --file=pylint/pylint.svg --value=$PYLINT_SCORE 2=red 4=orange 8=yellow 10=green
    - echo "Pylint score is $PYLINT_SCORE"
  artifacts:
    paths:
      - ./pylint/

Flake8 Lint:
  stage: Linting
  script:
    - nox -s flake8_code

Deploy to Mercury:
  stage: Deploy to Server
  only:
    - deploy
  dependencies:
    - PyLint Lint
  before_script:
    - 'which ssh-agent || ( apt-get install -qq openssh-client )'
    - eval $(ssh-agent -s)
    - ssh-add <(echo "$SSH_PRIVATE_KEY")
    - mkdir -p ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config'
  script:
    - ssh -o LogLevel=ERROR $SSH_USERNAME@$SSH_HOST "cd /srv/maybax/bot; git pull origin deploy; pyenv activate maybax; pip install -r requirements.txt; pyenv deactivate; sudo systemctl restart maybax"
